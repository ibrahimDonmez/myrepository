﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication4.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        [Route("Giris")]
        public ActionResult Giris()
        {
            DateTime Zaman = DateTime.Now;
            ViewBag.Tarih = Zaman;
            return View();
        }
        [Route("Kitaplar")]
        public ActionResult Kitaplar()
        {
            return View();
        }
        [Route("CalismaSaat")]
        public ActionResult CalismaSaat()
        {
            return View();
        }
    }
}